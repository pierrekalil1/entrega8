from http import HTTPStatus
import json
from json.decoder import JSONDecodeError

from app.exc import InvalidKeyError

class User():

    def __init__(self, name: str, email: str):
        
        self.name = name.title()
        self.email = email.lower()
    

    def validate(**kwargs):
        required_keys = ['name', 'email']
        if type(kwargs['name']) != str or type(kwargs['email']) != str:
            raise InvalidKeyError(kwargs['name'], kwargs['email'])
        

    @staticmethod
    def get_all():
        with open('app/database/database.json', 'r') as json_file:
            return json.load(json_file)
        
    
    def save(self):
        try:
            with open('app/database/database.json', 'r') as json_file:
                result = json.load(json_file)

        except JSONDecodeError:
            result = []
        
   
        for i in result:
            if self.email == i["email"]:
                return {"error": "User already exists."}, HTTPStatus.CONFLICT
    

        self.id = len(result) + 1
        result.append(self.__dict__)

        with open('app/database/database.json', 'w') as json_file:
            json.dump(result, json_file)

        return self.__dict__, HTTPStatus.CREATED
    

       