from flask import Flask, request, jsonify
import os
import json
from json import load, dump
from http import HTTPStatus
from app.classes import User
from app.exc import InvalidKeyError


app = Flask(__name__)
DATABASE = os.environ.get('DATABASE')

os.makedirs(DATABASE, exist_ok=True)
try:
    if not os.path.isfile('app/database/database.json'):
            with open('app/database/database.json', 'w') as json_file:
                json_data = []
                json.dump(json_data, json_file, indent=4)
except:
    pass


@app.get('/user')
def user():
   users_list = User.get_all()
   return jsonify(users_list), HTTPStatus.OK


@app.post('/user')
def create_user():
    data = request.json

    try:
        User.validate(**data)
    except InvalidKeyError as erro:
        return erro.message

    user = User(**data)
    result = user.save()

    return result
    