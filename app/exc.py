class InvalidKeyError(Exception):
    tipos = {
        str: "string",
        int: "integer",
        float: "float",
        list: "list",
        dict: "dictionary",
        bool: "boolean",
    }
    def __init__(self, name, email):
        self.message = {
            "wrong fields": [
                {"name": f"{self.tipos[type(name)]}"},
                {"email": f"{self.tipos[type(email)]}"}
            ]
        }